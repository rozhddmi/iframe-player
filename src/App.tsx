import React from "react";
import "./App.css";
import AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { parse } from "query-string";
const Player: React.FunctionComponent<{ match: any; location: any }> = ({
  match,
  location,
}) => {
  const parsed = parse(location.search);
  return (
    <div>
      <p>{JSON.stringify(parsed)}</p>
      {parsed.url ? (
        <AudioPlayer
          src={parsed.url as any}
          autoPlay
          crossOrigin="anonymous"
          loop
        />
      ) : (
        "No url"
      )}
    </div>
  );
};

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Player} />
      </Switch>
    </Router>
  );
}

export default App;
